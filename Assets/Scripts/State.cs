﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State : MonoBehaviour
{
    #region State

    [System.NonSerialized]
    private bool isEnabled = false;
    public bool IsEnabled
    {
        get => isEnabled;
        set => isEnabled = value;
    }

    #endregion State

    #region Methods

    public void Open()
    {
        gameObject.SetActive(isEnabled = true);
    }

    public void Close()
    {
        gameObject.SetActive(isEnabled = false);
    }

    #endregion Methods
}
