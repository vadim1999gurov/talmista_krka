﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Stater : MonoBehaviour
{
    #region Singleton

    public static Stater Instance = null;

    #endregion Singleton


    #region Parametes

    [SerializeField]
    private List<State> states = new List<State>();
    private List<State> States
    {
        get => states;
        set => states = value;
    }

    [SerializeField]
    private State firstState;
    public State FirstState
    {
        get => firstState;
        set => firstState = value;
    }

    #endregion Parametes

    #region Methods

    public void SetState(int stateNum)
    {
        if (stateNum >= states.Count || stateNum < 0) Debug.LogError("IndexError: set index out of range!");
        else
        {
            states[stateNum].Open();
        }
    }

    public void OpenState(string stateName)
    {
        try
        {
            int index = states.IndexOf(states.Single(el => el.name == stateName));
            states[index].Open();
        }
        catch
        {
            Debug.LogError($"NameError: State with {stateName} not be founde!");
        }
    }

    public void CloseState(int stateNum)
    {
        if (stateNum >= states.Count || stateNum < 0) Debug.LogError("IndexError: set index out of range!");
        else
        {
            states[stateNum].Close();
        }
    }

    public void CloseState(string stateName)
    {
        try
        {
            int index = states.IndexOf(states.Single(el => el.name == stateName));
            states[index].Close();
        }
        catch
        {
            Debug.LogError($"NameError: State with {stateName} not be founde!");
        }
    }

    private void CloseAllState()
    {
        foreach (var el in states) el.Close();
    }

    #endregion Methods

    #region Unity

    private void Awake()
    {
        if (Instance == null) Instance = this;

        CloseAllState();
    }

    #endregion Unity
}
