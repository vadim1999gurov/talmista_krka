﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StartMetering : MonoBehaviour
{
    #region Singleton

    public static StartMetering Instance = null;

    #endregion Singleton

    #region State

    private TMP_Dropdown dropdown = null;
    public TMP_Dropdown Dropdown => dropdown ?? (dropdown = GetComponentInChildren<TMP_Dropdown>());

    #endregion State

    #region Unity

    private void Start()
    {
        try
        {
            if (Instance == null) Instance = this;
            GetComponent<Button>().onClick.AddListener(OpenMetering);
            this.gameObject.SetActive(false);
        }
        catch
        {
            Debug.LogWarning($"{name}: button component not found");
        }
    }

    #endregion Unity

    #region Methods

    public void OpenMetering()
    {
        Stater.Instance.OpenState("Metering");
        TonometrAnimation.Instance.StartAnimation(false);
        if (Dropdown.interactable) Dropdown.interactable = false;
    }

    #endregion Methods
}
