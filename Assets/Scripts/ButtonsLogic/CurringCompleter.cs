﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurringCompleter : MonoBehaviour
{
    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(Compleat);
    }

    private void Compleat()
    {
        TonometrAnimation.Instance.GetComponentInParent<UIMoverDOT>().Return();
        StartMetering.Instance.GetComponent<UIMoverDOT>().Return();
        WomanController.Instance.SetState(AnimationState.StandUp, delegate
        {
            WomanController.Instance.SetState(AnimationState.Walk);
            WomanController.Instance.transform.localPosition = new Vector3(WomanController.Instance.transform.localPosition.x, WomanController.Instance.transform.localPosition.y, -0.6f);
            WomanController.Instance.Mover.StartWalking(new Vector3(0.5f, -0.5f, -3), delegate
            {
                WomanController.Instance.SetState(AnimationState.Idly);
                Exit();
            });
        });
        CorrectPillsScreen.Instance.gameObject.SetActive(false);
    }

    public void Exit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
    }
}