﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PillsChanger : MonoBehaviour
{
    #region Parameters

    [SerializeField]
    private int currentId = 0;
    public int CurrentId
    {
        get => currentId;
        set => currentId = value;
    }

    #endregion Parameters

    #region Methods

    private void SelectThisPills()
    {
        PatientData.Instance.SelctedPillsId = CurrentId;
        Stater.Instance.OpenState("CorrectPills");
        GetComponentsInParent<UIMoverDOT>()[0].Return(delegate { Stater.Instance.CloseState("PillsMenu"); });
    }

    #endregion Methods

    #region Untiy

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(SelectThisPills);
    }

    #endregion Unity
}
