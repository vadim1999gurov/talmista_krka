﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;

public class InfoPlaceholder : MonoBehaviour
{
    #region Parameters

    [SerializeField]
    private new TextMeshProUGUI name;
    public TextMeshProUGUI Name
    {
        get => (name ?? (name = GetComponentsInChildren<TextMeshProUGUI>().Single(el => el.name == "NameValue")));
        set => name = value;
    }

    [SerializeField]
    private TextMeshProUGUI surname;
    public TextMeshProUGUI Surname
    {
        get => (surname ?? (surname = GetComponentsInChildren<TextMeshProUGUI>().Single(el => el.name == "SurnameValue")));
        set => surname = value;
    }

    [SerializeField]
    private TextMeshProUGUI pantonymic;
    public TextMeshProUGUI Pantonymic
    {
        get => (pantonymic ?? (pantonymic = GetComponentsInChildren<TextMeshProUGUI>().Single(el => el.name == "PantonymicValue")));
        set => pantonymic = value;
    }

    [SerializeField]
    private TextMeshProUGUI age;
    public TextMeshProUGUI Age
    {
        get => (age ?? (age = GetComponentsInChildren<TextMeshProUGUI>().Single(el => el.name == "AgeValue")));
        set => age = value;
    }

    [SerializeField]
    private TextMeshProUGUI diagnosis;
    public TextMeshProUGUI Diagnosis
    {
        get => (diagnosis ?? (diagnosis = GetComponentsInChildren<TextMeshProUGUI>().Single(el => el.name == "Diagnosis")));
        set => diagnosis = value;
    }

    [SerializeField]
    private TextMeshProUGUI disease;
    public TextMeshProUGUI Disease
    {
        get => (disease ?? (disease = GetComponentsInChildren<TextMeshProUGUI>().Single(el => el.name == "Disease")));
        set => disease = value;
    }

    [SerializeField]
    private TextMeshProUGUI history;
    public TextMeshProUGUI History
    {
        get => (history ?? (history = GetComponentsInChildren<TextMeshProUGUI>().Single(el => el.name == "History")));
        set => history = value;
    }

    #endregion Parameters

    #region Unity

    private void OnEnable()
    {
        if (PatientData.Instance != null)
        {
            Name.text = PatientData.Instance.Name;
            Surname.text = PatientData.Instance.Surname;
            Pantonymic.text = PatientData.Instance.Patronymic;
            Age.text = PatientData.Instance.Age.ToString();

            diagnosis.text = PatientData.Instance.DataCase.diagnosis;
            Disease.text = PatientData.Instance.DataCase.disease;
            History.text = PatientData.Instance.DataCase.history;
        }
    }

    #endregion Unity
}
