﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CorrectPillsScreen : MonoBehaviour
{
    public static CorrectPillsScreen Instance = null;

    #region Parameters

    [SerializeField]
    private TextMeshProUGUI argument;
    public TextMeshProUGUI Argument
    {
        get => (argument ?? (argument = GetComponentsInChildren<TextMeshProUGUI>().Single(el => el.name == "Argument")));
        set => argument = value;
    }

    [SerializeField]
    private TextMeshProUGUI header;
    public TextMeshProUGUI Header
    {
        get => (header ?? (header = GetComponentsInChildren<TextMeshProUGUI>().Single(el => el.name == "Header")));
        set => header = value;
    }

    [SerializeField]
    private Image pillsImage;
    public Image PillsImage
    {
        get => (pillsImage ?? (pillsImage = GetComponentsInChildren<Image>().Single(el => el.name == "Pills")));
        set => pillsImage = value;
    }

    #endregion Parameters

    #region State

    private GameObject shadow;

    #endregion State

    #region Unity

    private void OnEnable()
    {
        if (Instance == null) Instance = this;
        if (shadow == null)
        {
            shadow = GetComponentInChildren<Image>().gameObject;
            shadow.SetActive(false);
        }


        if (PatientData.Instance != null && PatientData.Instance.DataCase != null)
        {
            Argument.text = PatientData.Instance.DataCase.argument;

            // Если выбор сделан правильно.
            if (PatientData.Instance.DataCase.rightPills.Any(el => el.id == PatientData.Instance.SelctedPillsId))
            {
                PillsImage.sprite = PatientData.Instance.DataCase.rightPills.Single(el => el.id == PatientData.Instance.SelctedPillsId).sprite;
                Header.gameObject.SetActive(false);
                WomanController.Instance.SetState(AnimationState.Curing, delegate { shadow.SetActive(true); });
                TonometrAnimation.Instance.time = 2;
                TonometrAnimation.Instance.startAngle = Vector3.forward * 70;
                TonometrAnimation.Instance.StartAnimation(true);
            }
            else
            {
                PillsImage.sprite = PatientData.Instance.DataCase.rightPills[0].sprite;
                Header.gameObject.SetActive(true);
                shadow.SetActive(true);
            }
        }
    }

    #endregion Unity
}
