﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DropdownController : MonoBehaviour
{
    #region State

    private TMP_Dropdown dropdown = null;
    public TMP_Dropdown Dropdown => dropdown ?? (dropdown = GetComponent<TMP_Dropdown>());

    #endregion State

    #region Methods

    private void ChangePatientData()
    {
        Debug.Log(Dropdown.value);
        PatientData.Instance.DataCase = PatientData.Instance.DataCases[Dropdown.value];
    }

    #endregion Methods

    #region Unity

    private void Start()
    {
        Dropdown.onValueChanged.AddListener(delegate { ChangePatientData(); });
    }

    #endregion Unity
}
