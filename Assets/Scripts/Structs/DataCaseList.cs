﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[CreateAssetMenu(fileName = "Data Case", menuName = "Structs/Data Case", order = 1)]
public class DataCaseList : ScriptableObject
{
    #region Parameters

    [SerializeField]
    private List<DataCase> dataCases = new List<DataCase>();
    public List<DataCase> DataCases
    {
        get => dataCases;
        set => dataCases = value;
    }

    #endregion Parameters

    #region Methods

    public DataCase GetRandomData()
    {
        if (dataCases.Count > 0) return dataCases[Random.Range((int)0, dataCases.Count)];
        return null;
    }

    #endregion Methods

    #region Indexer

    public DataCase this[int i] => DataCases[i];

    #endregion Indexer
}
