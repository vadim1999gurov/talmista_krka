﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatientData : MonoBehaviour
{
    #region Singleton

    public static PatientData Instance = null;

    #endregion Singleton

    #region Parameters

    [SerializeField]
    private DictionaryObject names;
    public DictionaryObject Names
    {
        get => names;
        set => names = value;
    }

    [SerializeField]
    private DictionaryObject surnames;
    public DictionaryObject Surnames
    {
        get => surnames;
        set => surnames = value;
    }

    [SerializeField]
    private DictionaryObject patronymics;
    public DictionaryObject Patronymics
    {
        get => patronymics;
        set => patronymics = value;
    }

    [SerializeField]
    private DataCaseList dataCases;
    public DataCaseList DataCases
    {
        get => dataCases;
        set => dataCases = value;
    }

    #endregion Parameters

    #region State

    [System.NonSerialized]
    private new string name;
    public string Name
    {
        get => name;
        set => name = value;
    }

    [System.NonSerialized]
    private string surname;
    public string Surname
    {
        get => surname;
        set => surname = value;
    }

    [System.NonSerialized]
    private string patronymic;
    public string Patronymic
    {
        get => patronymic;
        set => patronymic = value;
    }

    [System.NonSerialized]
    private int age;
    public int Age
    {
        get => age;
        set => age = value;
    }

    [System.NonSerialized]
    private DataCase dataCase;
    public DataCase DataCase
    {
        get => dataCase;
        set => dataCase = value;
    }

    [System.NonSerialized]
    private int selctedPillsId;
    public int SelctedPillsId
    {
        get => selctedPillsId;
        set => selctedPillsId = value;
    }



    #endregion State

    #region Methods

    private void LoadFullName()
    {
        name = Names.GetRandomData();
        surname = Surnames.GetRandomData();
        patronymic = Patronymics.GetRandomData();

        Debug.Log($"{surname} {name} {patronymic}");
    }

    #endregion Methods

    #region Unity

    private void Awake()
    {
        if (Instance == null) Instance = this;
    }

    private void Start()
    {
        LoadFullName();
        age = Random.Range((int)30, (int)45);
        dataCase = DataCases.DataCases[0];
    }

    #endregion Unity
}
