﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataCase
{
    #region Struct

    [System.Serializable]
    public class Pills
    {
        public int id;
        public Sprite sprite;
    }

    #endregion Struct

    #region Parameters

    [SerializeField]
    [TextArea(1, 2)]
    // Заболевание.
    public string disease;
    public string Disease
    {
        get => disease;
        set => disease = value;
    }

    [SerializeField]
    [TextArea(3, 5)]
    // Диагноз.
    public string diagnosis;
    public string Diagnosis
    {
        get => diagnosis;
        set => diagnosis = value;
    }

    [SerializeField]
    [TextArea(3, 5)]
    // История.
    public string history;
    public string History
    {
        get => history;
        set => history = value;
    }

    [SerializeField]
    // Id всех подходящих лекарств.
    public Pills[] rightPills;
    public Pills[] RightPills
    {
        get => rightPills;
        set => rightPills = value;
    }

    [SerializeField]
    [TextArea(3, 5)]
    // Причина, по которой данные препараты подходят.
    public string argument;
    public string Argument
    {
        get => argument;
        set => argument = value;
    }

    [SerializeField]
    private int systolicPressure = 120;
    public int SystolicPressure => systolicPressure;

    [SerializeField]
    private int diastolicPressure = 80;
    public int DiastolicPressure => diastolicPressure;

    #endregion Parameters
}
