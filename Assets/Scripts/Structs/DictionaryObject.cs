﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DictionaryObject", menuName = "Structs/Dictionary", order = 2)]
public class DictionaryObject : ScriptableObject
{
    #region Parameters

    [SerializeField]
    private List<string> dictionary = new List<string>();
    public List<string> Dictionary
    {
        get => dictionary;
        set => dictionary = value;
    }

    #endregion Parameters

    #region Methods

    public string GetRandomData()
    {
        if (dictionary.Count > 0) return dictionary[Random.Range((int)0, dictionary.Count)];
        return "";
    }

    #endregion Methods
}
