﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;


public class UIMoverDOT : MonoBehaviour
{
    enum StartState
    {
        Awake,
        Start,
        OnEnable,
        Call
    }

    #region State

    #region Global

    public static bool animationDone = false;

    #endregion Global

    [System.NonSerialized]
    private RectTransform rect;
    public RectTransform Rect
    {
        get
        {
            return rect ?? (rect = GetComponent<RectTransform>());
        }
        set => rect = value;
    }

    #endregion Global State

    #region Parameters

    [SerializeField]
    private float timeAnimation;
    public float TimeAnimation
    {
        get
        {
            return timeAnimation;
        }
    }

    [SerializeField]
    private float timeReturnAnimation = 1;
    public float TimeReturnAnimation
    {
        get
        {
            return timeReturnAnimation;
        }
    }

    [SerializeField]
    private Vector2 endPosition;
    public Vector2 EndPosition
    {
        get
        {
            return endPosition;
        }
    }

    [SerializeField]
    [Header("Optional")]
    private Vector2 beginPosition;
    public Vector2 BeginPosition
    {
        get
        {
            return beginPosition;
        }
    }

    [SerializeField]
    private StartState state = StartState.OnEnable;

    #endregion Parameters

    #region State

    private static List<UIMoverDOT> all = new List<UIMoverDOT>();
    public static List<UIMoverDOT> All
    {
        get
        {
            return all;
        }
    }

    #endregion State

    #region Delegates

    public delegate void Call();

    #endregion Delegates

    #region Events

    public static event Call OnStartAnimation;
    public static event Call OnEndAnimation;

    #endregion Events

    #region Methods

    public static void ReternAll()
    {
        if (OnStartAnimation != null) OnStartAnimation.Invoke();
        foreach (var el in All) el.GetComponent<RectTransform>().DOAnchorPos(el.BeginPosition, el.TimeReturnAnimation).SetUpdate(true).OnComplete(delegate { animationDone = true; if (OnEndAnimation != null) OnEndAnimation.Invoke(); });
    }

    public void Return(Call call = null)
    {
        if (OnStartAnimation != null) OnStartAnimation.Invoke();
        GetComponent<RectTransform>().DOAnchorPos(beginPosition, TimeReturnAnimation).SetUpdate(true).OnComplete(delegate { animationDone = true; call(); if (OnEndAnimation != null) OnEndAnimation.Invoke(); });

    }

    public void StartAnimation(Vector2 target, float time, Call call = null)
    {
        if (OnStartAnimation != null) OnStartAnimation.Invoke();
        GetComponent<RectTransform>().DOAnchorPos(target, time).SetUpdate(true).OnComplete(delegate { animationDone = true; call(); if (OnEndAnimation != null) OnEndAnimation.Invoke(); });
    }

    public void StartAnimation(Call call = null)
    {
        if (OnStartAnimation != null) OnStartAnimation.Invoke();
        GetComponent<RectTransform>().DOAnchorPos(EndPosition, TimeAnimation).SetUpdate(true).OnComplete(delegate { animationDone = true; call(); if (OnEndAnimation != null) OnEndAnimation.Invoke(); });
    }

    #endregion Methods

    #region Unity

    private void OnEnable()
    {
        if (!All.Contains(this)) All.Add(this);
        if (BeginPosition != Vector2.zero) GetComponent<RectTransform>().anchoredPosition = BeginPosition;
        else beginPosition = GetComponent<RectTransform>().anchoredPosition;

        if (state == StartState.OnEnable) StartAnimation();


    }

    private void OnDestroy()
    {
        All.Remove(this);
    }

    #endregion Unity
}

