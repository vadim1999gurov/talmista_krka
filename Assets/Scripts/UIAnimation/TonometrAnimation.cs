﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class TonometrAnimation : MonoBehaviour
{
    public static TonometrAnimation Instance = null;

    [SerializeField]
    public Vector3 startAngle;

    [SerializeField]
    private Vector3 endAngle;

    [SerializeField]
    public float time = 2f;

    [SerializeField]
    private RectTransform rotatedObject;

    [SerializeField]
    private TextMeshProUGUI text;

    [SerializeField]
    private bool isStart = false;
    private bool isFirstStartAnimation = true;

    [SerializeField]
    private int normalSystolicPressure = 120;
    public int NormalSystolicPressure => normalSystolicPressure;

    [SerializeField]
    private int normalDiastolicPressure = 80;
    public int NormalDiastolicPressure => normalDiastolicPressure;

    bool isReverse = false;

    public void StartAnimation(bool isReverseAnimation)
    {
        isReverse = isReverseAnimation;
        isStart = true;
        rotatedObject.DORotate((!isReverseAnimation ? startAngle : endAngle), 0, RotateMode.Fast);
        StartMetering.Instance.GetComponent<Button>().interactable = false;
        if (isStart && isFirstStartAnimation)
        {
            GetComponentsInParent<UIMoverDOT>()[0].StartAnimation(delegate { rotatedObject.DORotate((!isReverseAnimation ? endAngle : startAngle), time, RotateMode.FastBeyond360).SetUpdate(true).OnComplete(delegate { StartMetering.Instance.GetComponent<Button>().interactable = true; Stater.Instance.OpenState("PillsMenu"); isStart = false; }); });
            isFirstStartAnimation = false;
        }
        else rotatedObject.DORotate((!isReverseAnimation ? endAngle : startAngle), time, RotateMode.FastBeyond360).SetUpdate(true).OnComplete(delegate { StartMetering.Instance.GetComponent<Button>().interactable = true; isStart = false; });
    }

    private void OnEnable()
    {
        if (Instance == null) Instance = this;
    }

    private void Update()
    {
        if (isStart)
        {
            text.text = $"{(int)Mathf.Lerp(PatientData.Instance.DataCase.SystolicPressure, NormalSystolicPressure, rotatedObject.rotation.z / endAngle.z)}/{(int)Mathf.Lerp(PatientData.Instance.DataCase.DiastolicPressure, NormalDiastolicPressure, rotatedObject.rotation.z / endAngle.z)}";
        }
        else text.text = (!isReverse ? $"{PatientData.Instance.DataCase.SystolicPressure}/{PatientData.Instance.DataCase.DiastolicPressure}" : $"{NormalSystolicPressure}/{NormalDiastolicPressure}");
    }
}
