﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    #region Delegates

    public delegate void Call();

    #endregion Delegates

    #region Parameters

    [SerializeField]
    public Vector3 target;

    [SerializeField]
    public float speed = 1;

    #endregion Parameters

    #region State

    private bool isWalk = false;

    private Call method;

    #endregion State

    public void StartWalking(Call call = null)
    {
        isWalk = true;
        method = call;
    }

    public void StartWalking(Vector3 targ, Call call = null)
    {
        isWalk = true;
        method = call;
        target = targ;
    }


    #region Unity

    private void Update()
    {
        if (isWalk)
        {
            //transform.LookAt(new Vector3(0, target.y, 0));
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, target, speed * Time.deltaTime);

            if ((transform.localPosition - target).sqrMagnitude <= (transform.localPosition.normalized - target.normalized).sqrMagnitude)
            {
                transform.localPosition = target;
                isWalk = false;
                if (method != null)
                {
                    method();
                    method = null;
                }
            }
        }
    }

    #endregion Unity
}