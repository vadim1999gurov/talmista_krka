﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class TargetTrigger : MonoBehaviour
{
    [SerializeField]
    private GameObject obj;

    [SerializeField]
    private Canvas mainUICanvas;

    [SerializeField]
    private Canvas lostMessageCanvas;

    private void Start()
    {
        DefaultTrackableEventHandler.OnDetected += delegate { obj.SetActive(true); mainUICanvas.enabled = true; lostMessageCanvas.enabled = false; };
        DefaultTrackableEventHandler.OnLosted += delegate { mainUICanvas.enabled = false; lostMessageCanvas.enabled = true; };
    }
}
