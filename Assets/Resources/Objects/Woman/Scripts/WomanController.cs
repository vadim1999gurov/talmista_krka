﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region Enum

public enum AnimationState
{
    Idly,
    Walk,
    Sit,
    Pain,
    Curing,
    StandUp
}

#endregion Enum

public class WomanController : MonoBehaviour
{
    #region Singleton  

    public static WomanController Instance = null;

    #endregion Singleton

    #region Delegates

    public delegate void Call();

    private event Call onEndAnimation;

    #endregion Delegates

    #region Parameters 

    [SerializeField]
    private AnimationState state = AnimationState.Idly;
    public AnimationState State
    {
        get => (AnimationState)Animator.GetInteger("State");
        set => Animator.SetInteger("State", (int)value);
    }

    #endregion Parameters 

    #region State

    private Animator animator;
    public Animator Animator
    {
        get
        {
            if (animator == null) return animator = GetComponent<Animator>();
            return animator;
        }

        set => animator = value;
    }

    private Mover mover;
    public Mover Mover
    {
        get
        {
            return mover ?? (mover = GetComponent<Mover>());
        }

        set => mover = value;
    }

    #endregion State

    #region Methods

    public IEnumerator Timer(Call call)
    {
        float time = 0;
        do
        {
            yield return null;
            time = (animator.GetCurrentAnimatorStateInfo(0).normalizedTime % 1);
        }
        while (time < 0.98f);
        if (call != null) call();
        if (onEndAnimation != null) onEndAnimation.Invoke();
    }

    public void SetState(AnimationState nextState, Call call = null)
    {
        State = nextState;
        StartCoroutine(Timer(call));
    }

    #endregion Methods

    #region Unity

    private void Awake()
    {
        if (Instance == null) Instance = this;

    }

    private void OnEnable()
    {
        State = AnimationState.Idly;
    }

    private void Start()
    {
        State = AnimationState.Walk;
        Mover.StartWalking(delegate
        {
            transform.localRotation = new Quaternion(0, 180, 0, 1);
            SetState(AnimationState.Sit, delegate
            {
                SetState(AnimationState.Pain, delegate
                {
                    StartMetering.Instance.gameObject.SetActive(true);
                });

            });
        });
    }

    #endregion Unity
}
