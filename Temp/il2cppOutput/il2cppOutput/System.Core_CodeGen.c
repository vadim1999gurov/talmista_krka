﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000003 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000004 System.Exception System.Linq.Error::NoMatch()
extern void Error_NoMatch_m96B9371C94C28A7C23CC8B8D25CC7B50734E1B14 ();
// 0x00000005 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000007 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,System.Int32,TResult>)
// 0x00000008 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,System.Int32,TResult>)
// 0x00000009 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x0000000A System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x0000000B System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000C System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000D TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000E System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::OfType(System.Collections.IEnumerable)
// 0x00000010 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::OfTypeIterator(System.Collections.IEnumerable)
// 0x00000011 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000012 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x00000013 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000014 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000015 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000016 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000017 TSource System.Linq.Enumerable::Single(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000018 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000019 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x0000001A System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001B System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001C System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001D System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001E System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000001F System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000020 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000021 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000022 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000023 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000024 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000025 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000026 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000027 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000028 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000029 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000002A System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000002B System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000002C System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000002D System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000002E System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000002F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000030 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000031 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000032 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000033 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000034 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000035 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000036 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000037 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000038 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000039 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000003A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000003B System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000003C System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x0000003D System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x0000003E System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000003F System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000040 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000041 System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000042 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000043 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000044 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000045 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000046 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000047 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000048 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000049 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000004A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000004B System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::.ctor(System.Int32)
// 0x0000004C System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::System.IDisposable.Dispose()
// 0x0000004D System.Boolean System.Linq.Enumerable_<SelectIterator>d__5`2::MoveNext()
// 0x0000004E System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::<>m__Finally1()
// 0x0000004F TResult System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000050 System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.IEnumerator.Reset()
// 0x00000051 System.Object System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.IEnumerator.get_Current()
// 0x00000052 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000053 System.Collections.IEnumerator System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000054 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000055 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000056 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x00000057 TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000058 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::.ctor(System.Int32)
// 0x00000059 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.IDisposable.Dispose()
// 0x0000005A System.Boolean System.Linq.Enumerable_<OfTypeIterator>d__97`1::MoveNext()
// 0x0000005B System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::<>m__Finally1()
// 0x0000005C TResult System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x0000005D System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerator.Reset()
// 0x0000005E System.Object System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerator.get_Current()
// 0x0000005F System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000060 System.Collections.IEnumerator System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000061 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x00000062 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x00000063 System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x00000064 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x00000065 TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000066 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x00000067 System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x00000068 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000069 System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000006A System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x0000006B System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000006C System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x0000006D System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000006E System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000006F System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000070 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x00000071 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000072 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x00000073 System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x00000074 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000075 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000076 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000077 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000078 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000079 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x0000007A System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x0000007B System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x0000007C System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x0000007D System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x0000007E System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x0000007F System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x00000080 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x00000081 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000082 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000083 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000084 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000085 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000086 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000087 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000088 System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x00000089 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000008A System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000008B System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000008C System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000008D System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x0000008E System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x0000008F System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000090 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000091 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000092 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000093 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000094 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000095 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000096 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000097 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000098 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000099 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x0000009A System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x0000009B System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000009C System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000009D System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x0000009E System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x0000009F System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x000000A0 System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x000000A1 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000A2 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000A3 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x000000A4 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000A5 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000A6 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000A7 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[167] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	Error_NoMatch_m96B9371C94C28A7C23CC8B8D25CC7B50734E1B14,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[167] = 
{
	0,
	19,
	19,
	19,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[55] = 
{
	{ 0x02000004, { 86, 4 } },
	{ 0x02000005, { 90, 9 } },
	{ 0x02000006, { 101, 7 } },
	{ 0x02000007, { 110, 10 } },
	{ 0x02000008, { 122, 11 } },
	{ 0x02000009, { 136, 9 } },
	{ 0x0200000A, { 148, 12 } },
	{ 0x0200000B, { 163, 9 } },
	{ 0x0200000C, { 172, 1 } },
	{ 0x0200000D, { 173, 2 } },
	{ 0x0200000E, { 175, 6 } },
	{ 0x0200000F, { 181, 6 } },
	{ 0x02000010, { 187, 2 } },
	{ 0x02000012, { 189, 3 } },
	{ 0x02000013, { 194, 5 } },
	{ 0x02000014, { 199, 7 } },
	{ 0x02000015, { 206, 3 } },
	{ 0x02000016, { 209, 7 } },
	{ 0x02000017, { 216, 4 } },
	{ 0x02000018, { 220, 34 } },
	{ 0x0200001A, { 254, 2 } },
	{ 0x06000005, { 0, 10 } },
	{ 0x06000006, { 10, 10 } },
	{ 0x06000007, { 20, 1 } },
	{ 0x06000008, { 21, 2 } },
	{ 0x06000009, { 23, 5 } },
	{ 0x0600000A, { 28, 5 } },
	{ 0x0600000B, { 33, 2 } },
	{ 0x0600000C, { 35, 1 } },
	{ 0x0600000D, { 36, 3 } },
	{ 0x0600000E, { 39, 2 } },
	{ 0x0600000F, { 41, 1 } },
	{ 0x06000010, { 42, 2 } },
	{ 0x06000011, { 44, 2 } },
	{ 0x06000012, { 46, 2 } },
	{ 0x06000013, { 48, 4 } },
	{ 0x06000014, { 52, 4 } },
	{ 0x06000015, { 56, 3 } },
	{ 0x06000016, { 59, 4 } },
	{ 0x06000017, { 63, 3 } },
	{ 0x06000018, { 66, 3 } },
	{ 0x06000019, { 69, 1 } },
	{ 0x0600001A, { 70, 1 } },
	{ 0x0600001B, { 71, 3 } },
	{ 0x0600001C, { 74, 3 } },
	{ 0x0600001D, { 77, 2 } },
	{ 0x0600001E, { 79, 2 } },
	{ 0x0600001F, { 81, 5 } },
	{ 0x0600002F, { 99, 2 } },
	{ 0x06000034, { 108, 2 } },
	{ 0x06000039, { 120, 2 } },
	{ 0x0600003F, { 133, 3 } },
	{ 0x06000044, { 145, 3 } },
	{ 0x06000049, { 160, 3 } },
	{ 0x0600006F, { 192, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[256] = 
{
	{ (Il2CppRGCTXDataType)2, 18832 },
	{ (Il2CppRGCTXDataType)3, 13227 },
	{ (Il2CppRGCTXDataType)2, 18833 },
	{ (Il2CppRGCTXDataType)2, 18834 },
	{ (Il2CppRGCTXDataType)3, 13228 },
	{ (Il2CppRGCTXDataType)2, 18835 },
	{ (Il2CppRGCTXDataType)2, 18836 },
	{ (Il2CppRGCTXDataType)3, 13229 },
	{ (Il2CppRGCTXDataType)2, 18837 },
	{ (Il2CppRGCTXDataType)3, 13230 },
	{ (Il2CppRGCTXDataType)2, 18838 },
	{ (Il2CppRGCTXDataType)3, 13231 },
	{ (Il2CppRGCTXDataType)2, 18839 },
	{ (Il2CppRGCTXDataType)2, 18840 },
	{ (Il2CppRGCTXDataType)3, 13232 },
	{ (Il2CppRGCTXDataType)2, 18841 },
	{ (Il2CppRGCTXDataType)2, 18842 },
	{ (Il2CppRGCTXDataType)3, 13233 },
	{ (Il2CppRGCTXDataType)2, 18843 },
	{ (Il2CppRGCTXDataType)3, 13234 },
	{ (Il2CppRGCTXDataType)3, 13235 },
	{ (Il2CppRGCTXDataType)2, 18844 },
	{ (Il2CppRGCTXDataType)3, 13236 },
	{ (Il2CppRGCTXDataType)2, 18845 },
	{ (Il2CppRGCTXDataType)3, 13237 },
	{ (Il2CppRGCTXDataType)3, 13238 },
	{ (Il2CppRGCTXDataType)2, 13342 },
	{ (Il2CppRGCTXDataType)3, 13239 },
	{ (Il2CppRGCTXDataType)2, 18846 },
	{ (Il2CppRGCTXDataType)3, 13240 },
	{ (Il2CppRGCTXDataType)3, 13241 },
	{ (Il2CppRGCTXDataType)2, 13349 },
	{ (Il2CppRGCTXDataType)3, 13242 },
	{ (Il2CppRGCTXDataType)2, 18847 },
	{ (Il2CppRGCTXDataType)3, 13243 },
	{ (Il2CppRGCTXDataType)3, 13244 },
	{ (Il2CppRGCTXDataType)2, 18848 },
	{ (Il2CppRGCTXDataType)3, 13245 },
	{ (Il2CppRGCTXDataType)3, 13246 },
	{ (Il2CppRGCTXDataType)2, 13364 },
	{ (Il2CppRGCTXDataType)3, 13247 },
	{ (Il2CppRGCTXDataType)3, 13248 },
	{ (Il2CppRGCTXDataType)2, 18849 },
	{ (Il2CppRGCTXDataType)3, 13249 },
	{ (Il2CppRGCTXDataType)2, 13369 },
	{ (Il2CppRGCTXDataType)3, 13250 },
	{ (Il2CppRGCTXDataType)2, 18850 },
	{ (Il2CppRGCTXDataType)3, 13251 },
	{ (Il2CppRGCTXDataType)2, 18851 },
	{ (Il2CppRGCTXDataType)2, 18852 },
	{ (Il2CppRGCTXDataType)2, 13373 },
	{ (Il2CppRGCTXDataType)2, 18853 },
	{ (Il2CppRGCTXDataType)2, 18854 },
	{ (Il2CppRGCTXDataType)2, 18855 },
	{ (Il2CppRGCTXDataType)2, 13375 },
	{ (Il2CppRGCTXDataType)2, 18856 },
	{ (Il2CppRGCTXDataType)2, 13377 },
	{ (Il2CppRGCTXDataType)2, 18857 },
	{ (Il2CppRGCTXDataType)3, 13252 },
	{ (Il2CppRGCTXDataType)2, 18858 },
	{ (Il2CppRGCTXDataType)2, 18859 },
	{ (Il2CppRGCTXDataType)2, 13380 },
	{ (Il2CppRGCTXDataType)2, 18860 },
	{ (Il2CppRGCTXDataType)2, 13382 },
	{ (Il2CppRGCTXDataType)2, 18861 },
	{ (Il2CppRGCTXDataType)3, 13253 },
	{ (Il2CppRGCTXDataType)2, 13385 },
	{ (Il2CppRGCTXDataType)2, 18862 },
	{ (Il2CppRGCTXDataType)3, 13254 },
	{ (Il2CppRGCTXDataType)2, 18863 },
	{ (Il2CppRGCTXDataType)2, 13390 },
	{ (Il2CppRGCTXDataType)2, 13392 },
	{ (Il2CppRGCTXDataType)2, 18864 },
	{ (Il2CppRGCTXDataType)3, 13255 },
	{ (Il2CppRGCTXDataType)2, 13395 },
	{ (Il2CppRGCTXDataType)2, 18865 },
	{ (Il2CppRGCTXDataType)3, 13256 },
	{ (Il2CppRGCTXDataType)2, 18866 },
	{ (Il2CppRGCTXDataType)2, 13398 },
	{ (Il2CppRGCTXDataType)2, 18867 },
	{ (Il2CppRGCTXDataType)3, 13257 },
	{ (Il2CppRGCTXDataType)3, 13258 },
	{ (Il2CppRGCTXDataType)2, 18868 },
	{ (Il2CppRGCTXDataType)2, 13402 },
	{ (Il2CppRGCTXDataType)2, 18869 },
	{ (Il2CppRGCTXDataType)2, 13404 },
	{ (Il2CppRGCTXDataType)3, 13259 },
	{ (Il2CppRGCTXDataType)3, 13260 },
	{ (Il2CppRGCTXDataType)2, 13407 },
	{ (Il2CppRGCTXDataType)3, 13261 },
	{ (Il2CppRGCTXDataType)3, 13262 },
	{ (Il2CppRGCTXDataType)2, 13419 },
	{ (Il2CppRGCTXDataType)2, 18870 },
	{ (Il2CppRGCTXDataType)3, 13263 },
	{ (Il2CppRGCTXDataType)3, 13264 },
	{ (Il2CppRGCTXDataType)2, 13421 },
	{ (Il2CppRGCTXDataType)2, 18755 },
	{ (Il2CppRGCTXDataType)3, 13265 },
	{ (Il2CppRGCTXDataType)3, 13266 },
	{ (Il2CppRGCTXDataType)2, 18871 },
	{ (Il2CppRGCTXDataType)3, 13267 },
	{ (Il2CppRGCTXDataType)3, 13268 },
	{ (Il2CppRGCTXDataType)2, 13431 },
	{ (Il2CppRGCTXDataType)2, 18872 },
	{ (Il2CppRGCTXDataType)3, 13269 },
	{ (Il2CppRGCTXDataType)3, 13270 },
	{ (Il2CppRGCTXDataType)3, 12875 },
	{ (Il2CppRGCTXDataType)3, 13271 },
	{ (Il2CppRGCTXDataType)2, 18873 },
	{ (Il2CppRGCTXDataType)3, 13272 },
	{ (Il2CppRGCTXDataType)3, 13273 },
	{ (Il2CppRGCTXDataType)2, 13443 },
	{ (Il2CppRGCTXDataType)2, 18874 },
	{ (Il2CppRGCTXDataType)3, 13274 },
	{ (Il2CppRGCTXDataType)3, 13275 },
	{ (Il2CppRGCTXDataType)3, 13276 },
	{ (Il2CppRGCTXDataType)3, 13277 },
	{ (Il2CppRGCTXDataType)3, 13278 },
	{ (Il2CppRGCTXDataType)3, 12881 },
	{ (Il2CppRGCTXDataType)3, 13279 },
	{ (Il2CppRGCTXDataType)2, 18875 },
	{ (Il2CppRGCTXDataType)3, 13280 },
	{ (Il2CppRGCTXDataType)3, 13281 },
	{ (Il2CppRGCTXDataType)2, 13456 },
	{ (Il2CppRGCTXDataType)2, 18876 },
	{ (Il2CppRGCTXDataType)3, 13282 },
	{ (Il2CppRGCTXDataType)3, 13283 },
	{ (Il2CppRGCTXDataType)2, 13458 },
	{ (Il2CppRGCTXDataType)2, 18877 },
	{ (Il2CppRGCTXDataType)3, 13284 },
	{ (Il2CppRGCTXDataType)3, 13285 },
	{ (Il2CppRGCTXDataType)2, 18878 },
	{ (Il2CppRGCTXDataType)3, 13286 },
	{ (Il2CppRGCTXDataType)3, 13287 },
	{ (Il2CppRGCTXDataType)2, 18879 },
	{ (Il2CppRGCTXDataType)3, 13288 },
	{ (Il2CppRGCTXDataType)3, 13289 },
	{ (Il2CppRGCTXDataType)2, 13473 },
	{ (Il2CppRGCTXDataType)2, 18880 },
	{ (Il2CppRGCTXDataType)3, 13290 },
	{ (Il2CppRGCTXDataType)3, 13291 },
	{ (Il2CppRGCTXDataType)3, 13292 },
	{ (Il2CppRGCTXDataType)3, 12892 },
	{ (Il2CppRGCTXDataType)2, 18881 },
	{ (Il2CppRGCTXDataType)3, 13293 },
	{ (Il2CppRGCTXDataType)3, 13294 },
	{ (Il2CppRGCTXDataType)2, 18882 },
	{ (Il2CppRGCTXDataType)3, 13295 },
	{ (Il2CppRGCTXDataType)3, 13296 },
	{ (Il2CppRGCTXDataType)2, 13489 },
	{ (Il2CppRGCTXDataType)2, 18883 },
	{ (Il2CppRGCTXDataType)3, 13297 },
	{ (Il2CppRGCTXDataType)3, 13298 },
	{ (Il2CppRGCTXDataType)3, 13299 },
	{ (Il2CppRGCTXDataType)3, 13300 },
	{ (Il2CppRGCTXDataType)3, 13301 },
	{ (Il2CppRGCTXDataType)3, 13302 },
	{ (Il2CppRGCTXDataType)3, 12898 },
	{ (Il2CppRGCTXDataType)2, 18884 },
	{ (Il2CppRGCTXDataType)3, 13303 },
	{ (Il2CppRGCTXDataType)3, 13304 },
	{ (Il2CppRGCTXDataType)2, 18885 },
	{ (Il2CppRGCTXDataType)3, 13305 },
	{ (Il2CppRGCTXDataType)3, 13306 },
	{ (Il2CppRGCTXDataType)2, 18886 },
	{ (Il2CppRGCTXDataType)2, 18887 },
	{ (Il2CppRGCTXDataType)3, 13307 },
	{ (Il2CppRGCTXDataType)3, 13308 },
	{ (Il2CppRGCTXDataType)2, 13506 },
	{ (Il2CppRGCTXDataType)2, 18888 },
	{ (Il2CppRGCTXDataType)3, 13309 },
	{ (Il2CppRGCTXDataType)3, 13310 },
	{ (Il2CppRGCTXDataType)3, 13311 },
	{ (Il2CppRGCTXDataType)3, 13312 },
	{ (Il2CppRGCTXDataType)3, 13313 },
	{ (Il2CppRGCTXDataType)3, 13314 },
	{ (Il2CppRGCTXDataType)2, 13530 },
	{ (Il2CppRGCTXDataType)3, 13315 },
	{ (Il2CppRGCTXDataType)2, 18889 },
	{ (Il2CppRGCTXDataType)3, 13316 },
	{ (Il2CppRGCTXDataType)3, 13317 },
	{ (Il2CppRGCTXDataType)3, 13318 },
	{ (Il2CppRGCTXDataType)2, 13538 },
	{ (Il2CppRGCTXDataType)3, 13319 },
	{ (Il2CppRGCTXDataType)2, 18890 },
	{ (Il2CppRGCTXDataType)3, 13320 },
	{ (Il2CppRGCTXDataType)3, 13321 },
	{ (Il2CppRGCTXDataType)2, 18891 },
	{ (Il2CppRGCTXDataType)2, 18892 },
	{ (Il2CppRGCTXDataType)2, 18893 },
	{ (Il2CppRGCTXDataType)3, 13322 },
	{ (Il2CppRGCTXDataType)3, 13323 },
	{ (Il2CppRGCTXDataType)2, 18894 },
	{ (Il2CppRGCTXDataType)3, 13324 },
	{ (Il2CppRGCTXDataType)2, 18895 },
	{ (Il2CppRGCTXDataType)3, 13325 },
	{ (Il2CppRGCTXDataType)3, 13326 },
	{ (Il2CppRGCTXDataType)3, 13327 },
	{ (Il2CppRGCTXDataType)2, 13567 },
	{ (Il2CppRGCTXDataType)3, 13328 },
	{ (Il2CppRGCTXDataType)2, 13575 },
	{ (Il2CppRGCTXDataType)3, 13329 },
	{ (Il2CppRGCTXDataType)2, 18896 },
	{ (Il2CppRGCTXDataType)2, 18897 },
	{ (Il2CppRGCTXDataType)3, 13330 },
	{ (Il2CppRGCTXDataType)3, 13331 },
	{ (Il2CppRGCTXDataType)3, 13332 },
	{ (Il2CppRGCTXDataType)3, 13333 },
	{ (Il2CppRGCTXDataType)3, 13334 },
	{ (Il2CppRGCTXDataType)3, 13335 },
	{ (Il2CppRGCTXDataType)2, 13591 },
	{ (Il2CppRGCTXDataType)2, 18898 },
	{ (Il2CppRGCTXDataType)3, 13336 },
	{ (Il2CppRGCTXDataType)3, 13337 },
	{ (Il2CppRGCTXDataType)2, 13595 },
	{ (Il2CppRGCTXDataType)3, 13338 },
	{ (Il2CppRGCTXDataType)2, 18899 },
	{ (Il2CppRGCTXDataType)2, 13605 },
	{ (Il2CppRGCTXDataType)2, 13603 },
	{ (Il2CppRGCTXDataType)2, 18900 },
	{ (Il2CppRGCTXDataType)3, 13339 },
	{ (Il2CppRGCTXDataType)2, 18901 },
	{ (Il2CppRGCTXDataType)3, 13340 },
	{ (Il2CppRGCTXDataType)3, 13341 },
	{ (Il2CppRGCTXDataType)2, 13612 },
	{ (Il2CppRGCTXDataType)3, 13342 },
	{ (Il2CppRGCTXDataType)2, 13612 },
	{ (Il2CppRGCTXDataType)3, 13343 },
	{ (Il2CppRGCTXDataType)2, 13629 },
	{ (Il2CppRGCTXDataType)3, 13344 },
	{ (Il2CppRGCTXDataType)3, 13345 },
	{ (Il2CppRGCTXDataType)3, 13346 },
	{ (Il2CppRGCTXDataType)2, 18902 },
	{ (Il2CppRGCTXDataType)3, 13347 },
	{ (Il2CppRGCTXDataType)3, 13348 },
	{ (Il2CppRGCTXDataType)3, 13349 },
	{ (Il2CppRGCTXDataType)2, 13609 },
	{ (Il2CppRGCTXDataType)3, 13350 },
	{ (Il2CppRGCTXDataType)3, 13351 },
	{ (Il2CppRGCTXDataType)2, 13614 },
	{ (Il2CppRGCTXDataType)3, 13352 },
	{ (Il2CppRGCTXDataType)1, 18903 },
	{ (Il2CppRGCTXDataType)2, 13613 },
	{ (Il2CppRGCTXDataType)3, 13353 },
	{ (Il2CppRGCTXDataType)1, 13613 },
	{ (Il2CppRGCTXDataType)1, 13609 },
	{ (Il2CppRGCTXDataType)2, 18902 },
	{ (Il2CppRGCTXDataType)2, 13613 },
	{ (Il2CppRGCTXDataType)2, 13611 },
	{ (Il2CppRGCTXDataType)2, 13615 },
	{ (Il2CppRGCTXDataType)3, 13354 },
	{ (Il2CppRGCTXDataType)3, 13355 },
	{ (Il2CppRGCTXDataType)3, 13356 },
	{ (Il2CppRGCTXDataType)2, 13610 },
	{ (Il2CppRGCTXDataType)3, 13357 },
	{ (Il2CppRGCTXDataType)2, 13625 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	167,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	55,
	s_rgctxIndices,
	256,
	s_rgctxValues,
	NULL,
};
